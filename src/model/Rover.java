package model;

import lejos.hardware.Sound;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.Color;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

/**
 * Represents a Rover that can interact with its environment.
 * 
 * @author Kathryn Browning
 * @author Destiny Harris
 * @date August 18, 2016
 * @version CS 3270 Intelligent Systems
 *
 */
public class Rover {

	private final int DEGREES_45_CALLIBATION = 600;

	private final double OFFSET_TURNING_DISTANCE = 0.05;

	private final int GREEN_CALLIBRATION_OFFSET = 6;

	private final int INITIAL_SPEED = 180;
	private final int MEDIUM_SPEED = 100;
	private final int SLOW_SPEED = 50;

	private EV3LargeRegulatedMotor leftMotor;
	private EV3LargeRegulatedMotor rightMotor;
	private EV3UltrasonicSensor frontEyeSensor;
	private EV3UltrasonicSensor leftEyeSensor;
	private EV3UltrasonicSensor rightEyeSensor;
	private EV3ColorSensor colorSensor;

	/**
	 * Constructs a Rover that is able to move, perceive depth, and detect
	 * colors.
	 * 
	 * @param theLeftMotorPort
	 *            the left motor
	 * @param theRightMotorPort
	 *            the right motor
	 * @param eyeSensorPort
	 *            the ultrasonic sensor
	 * @param theColorSensorPort
	 *            the color sensor
	 */
	public Rover(Port theLeftMotorPort, Port theRightMotorPort, Port theFrontEyeSensor, Port theLeftEyeSensor,
			Port theRightEyeSensor, Port theColorSensorPort) {

		this.leftMotor = new EV3LargeRegulatedMotor(theLeftMotorPort);
		this.rightMotor = new EV3LargeRegulatedMotor(theRightMotorPort);
		this.frontEyeSensor = new EV3UltrasonicSensor(theFrontEyeSensor);
		this.leftEyeSensor = new EV3UltrasonicSensor(theLeftEyeSensor);
		this.rightEyeSensor = new EV3UltrasonicSensor(theRightEyeSensor);
		this.colorSensor = new EV3ColorSensor(theColorSensorPort);

		this.setSpeed(INITIAL_SPEED);
	}

	private void sleep(int value) {
		Delay.msDelay(value);
	}

	public void run() {
		boolean shouldKeepMoving = true;

		while (shouldKeepMoving) {
			
			this.moveForward();
			this.setSpeed(INITIAL_SPEED);
			
			int colorPerceived = this.colorSensor.getColorID();

			if (colorPerceived == Color.BLUE) {
				shouldKeepMoving = false;
			}

			this.reactToColor(colorPerceived);
			
			if (this.isApproachingObstacle()) {

				this.avoidObstacle();
			} 
		}

		this.stopMoving();
	}

	private boolean isApproachingObstacle() {
		boolean approachingObstacle = false;
		EV3UltrasonicSensor[] obstacleDetecters = { this.frontEyeSensor, this.leftEyeSensor, this.rightEyeSensor };

		for (EV3UltrasonicSensor eyeSensor : obstacleDetecters) {
			if (this.detectedObstacleFrom(eyeSensor)) {
				approachingObstacle = true;
			}
		}
		return approachingObstacle;
	}

	private boolean detectedObstacleFrom(EV3UltrasonicSensor eyeSensor) {
		return this.getDistanceTillApproachObstacle(eyeSensor) <= OFFSET_TURNING_DISTANCE;
	}

	private float getDistanceTillApproachObstacle(EV3UltrasonicSensor eyeSensor) {
		SampleProvider sensor = eyeSensor.getDistanceMode();
		return this.getSampleFrom(sensor, 0);
	}

	private float getSampleFrom(SampleProvider sensor, int perceptIndex) {
		float[] sample = new float[sensor.sampleSize()];
		sensor.fetchSample(sample, 0);
		return sample[perceptIndex];
	}

	private void avoidObstacle() {

		this.moveBackward();
		Direction direction = determioneDirectionToMove();

		if (direction == Direction.Right) {
			this.turnRight();
		} else {
			this.turnLeft();
		}
		this.moveForward();
	}

	private Direction determioneDirectionToMove() {
		Direction decidedDirection = Direction.Left;

		if (this.getDistanceTillApproachObstacle(this.leftEyeSensor) < this
				.getDistanceTillApproachObstacle(this.rightEyeSensor)) {
			decidedDirection = Direction.Right;
		}
		return decidedDirection;
	}

	private void moveBackward() {
		this.leftMotor.backward();
		this.rightMotor.backward();
	}

	private void moveForward() {
		this.leftMotor.forward();
		this.rightMotor.forward();
	}

	private void turnLeft() {
		this.turnLeft(DEGREES_45_CALLIBATION);
	}

	private void turnRight() {
		turnRight(DEGREES_45_CALLIBATION);
	}

	private void turnLeft(int degrees) {
		this.leftMotor.stop();
		this.rightMotor.forward();
		sleep(degrees);
	}

	private void turnRight(int degrees) {
		this.leftMotor.forward();
		this.rightMotor.stop();
		sleep(degrees);
	}

	private void reactToColor(int colorPerceived) {

		if (colorPerceived <= Color.GREEN + GREEN_CALLIBRATION_OFFSET && colorPerceived >= GREEN_CALLIBRATION_OFFSET) {
			this.reactToGreen();
			this.moveForward();
		} else if (colorPerceived == Color.RED) {
			this.reactToRed();
			this.turnRight();
			this.moveForward();
		} else if (colorPerceived == Color.BLUE) {
			this.reactToBlue();
			this.stopMoving();
		} 
	}

	private void reactToGreen() {
		this.setSpeed(MEDIUM_SPEED);
	}

	private void setSpeed(int speed) {
		this.leftMotor.setSpeed(speed);
		this.rightMotor.setSpeed(speed);

	}

	private void reactToRed() {
		this.beepNonRepeatively();
		this.setSpeed(SLOW_SPEED);
	}

	private void reactToBlue() {
		this.beepRepeatively();
	}

	private void beepNonRepeatively() {
		Sound.setVolume(7);
		Sound.beep();
	}

	public void beepRepeatively() {
		Sound.setVolume(7);
		Sound.beepSequenceUp();
	}

	private void stopMoving() {
		this.leftMotor.stop();
		this.rightMotor.stop();
	}

}