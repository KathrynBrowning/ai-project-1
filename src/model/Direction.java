/**
 * 
 */
package model;

/**
 * @author csuser
 *
 */
public enum Direction {

	Right,
	Left,
	Forward,
	Backward
}
