/**
 * 
 */
package controller;

import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import model.Rover;

/**
 * @author Kathryn Browning
 * @author Destiny Harris
 * @date August 18, 2016
 * @version CS 3270 Intelligent Systems
 *
 */
public class Controller {

	/**
	 * Runs JuneBug
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		Rover juneBug = new Rover(MotorPort.A, MotorPort.D, SensorPort.S4, SensorPort.S2, SensorPort.S3, SensorPort.S1);
		juneBug.run();
	}
}